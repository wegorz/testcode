#include <iostream>

/*
using namespace std;


// C++ program to find the shortest path between
// a given source cell to a destination cell.
#include <bits/stdc++.h>
using namespace std;
#define ROW 9
#define COL 10

//To store matrix cell cordinates
struct Point
{
    int x;
    int y;
};

// A Data Structure for queue used in BFS
struct queueNode
{
    Point pt;  // The cordinates of a cell
    int dist;  // cell's distance of from the source
};

// check whether given cell (row, col) is a valid
// cell or not.
bool isValid(int row, int col)
{
    // return true if row number and column number
    // is in range
    return (row >= 0) && (row < ROW) &&
            (col >= 0) && (col < COL);
}

// These arrays are used to get row and column
// numbers of 4 neighbours of a given cell
int rowNum[] = {-1, 0, 0, 1};
int colNum[] = {0, -1, 1, 0};

// function to find the shortest path between
// a given source cell to a destination cell.
int BFS(int mat[][COL], Point src, Point dest)
{
    // check source and destination cell
    // of the matrix have value 1
    if (!mat[src.x][src.y] || !mat[dest.x][dest.y])
        return -1;

    bool visited[ROW][COL];
    memset(visited, false, sizeof visited);

    // Mark the source cell as visited
    visited[src.x][src.y] = true;

    // Create a queue for BFS
    queue<queueNode> q;

    // Distance of source cell is 0
    queueNode s = {src, 0};
    q.push(s);  // Enqueue source cell

    // Do a BFS starting from source cell
    while (!q.empty())
    {
        queueNode curr = q.front();
        Point pt = curr.pt;

        // If we have reached the destination cell,
        // we are done
        if (pt.x == dest.x && pt.y == dest.y)
            return curr.dist;

        // Otherwise dequeue the front cell in the queue
        // and enqueue its adjacent cells
        q.pop();

        for (int i = 0; i < 4; i++)
        {
            int row = pt.x + rowNum[i];
            int col = pt.y + colNum[i];

            // if adjacent cell is valid, has path and
            // not visited yet, enqueue it.
            if (isValid(row, col) && mat[row][col] &&
                    !visited[row][col])
            {
                // mark cell as visited and enqueue it
                visited[row][col] = true;
                queueNode Adjcell = { {row, col},
                                      curr.dist + 1 };
                q.push(Adjcell);
            }
        }
    }

    // Return -1 if destination cannot be reached
    return -1;
}

// Driver program to test above function
int main()
{
    int mat[ROW][COL] =
    {
        { 1, 0, 1, 1, 1, 1, 0, 1, 1, 1 },
        { 1, 0, 1, 0, 1, 1, 1, 0, 1, 1 },
        { 1, 1, 1, 0, 1, 1, 0, 1, 0, 1 },
        { 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 },
        { 1, 1, 1, 0, 1, 1, 1, 0, 1, 0 },
        { 1, 0, 1, 1, 1, 1, 0, 1, 0, 0 },
        { 1, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
        { 1, 0, 1, 1, 1, 1, 0, 1, 1, 1 },
        { 1, 1, 0, 0, 0, 0, 1, 0, 0, 1 }
    };

    Point source = {0, 0};
    Point dest = {3, 4};

    int dist = BFS(mat, source, dest);

    if (dist != INT_MAX)
        cout << "Shortest Path is " << dist ;
    else
        cout << "Shortest Path doesn't exist";

    return 0;
}
*/

#include <algorithm>
#include <string>
#include <queue>
#include <vector>
#include <iostream>
#include <fstream>
#include <set>

using namespace std;

void print2D(vector<vector<int>> arr)
{
    for(int i=0;i<arr.size();++i)
    {
        for(int j=0;j<arr[0].size();++j)
        {
            printf("%3d ", (arr[i][j] == (1<<30) ? -9 : arr[i][j]));
        }
        printf("\n");
    }
}

void solve(vector<vector<int>>& maze, int si, int sj, int ei, int ej)
{
    queue<pair<int,int>> Q;
    set<pair<int,int>> S;
    int M = maze.size(), N = maze[0].size();
    S.insert(make_pair(si,sj));
    Q.push(make_pair(si,sj));

    while (!Q.empty()) {
        pair<int,int> p = Q.front();
        //cout << "P" << p.first << ":" << p.second << endl;
        Q.pop();
        for (int x : {-2, 0, 2}) {
            for (int y : {-2, 0, 2}) {
                if (x == 0 && y == 0)
                    continue;

                int i1 = p.first + x / 2;
                int j1 = p.second + y / 2;
                if (i1 < 0 || i1 >= M || j1 < 0 || j1 >= N || maze[i1][j1] == -1)
                    continue;

                int i = p.first + x;
                int j = p.second + y;
                if (i < 0 || i >= M || j < 0 || j >= N || maze[i][j] == -1)
                    continue;

                maze[i1][j1] = min(maze[i1][j1], maze[p.first][p.second] + 1);
                maze[i][j] = min(maze[i][j], maze[p.first][p.second] + 1);

                if (S.find(make_pair(i,j)) == S.end())
                {
                    S.insert(make_pair(i,j));
                    Q.push(make_pair(i,j));
                }
            }
        }
    }

    int start = maze[ei][ej];
    int pi = ei;
    int pj = ej;
    string path = "";
    while (start) {
        if (maze[pi-1][pj] == start) {
            path += "S";
            pi -= 2;
        }
        else if (maze[pi+1][pj] == start) {
            path += "N";
            pi += 2;
        }
        else if (maze[pi][pj-1] == start) {
            path += "E";
            pj -= 2;
        }
        else if (maze[pi][pj+1] == start) {
            path += "W";
            pj += 2;
        }
        start--;
    }
    reverse(path.begin(), path.end());
    printf("%s\n", path.c_str(), path.length());
}

int main(int argc, char** argv)
{
    ++argv;
    ifstream infile(*argv);
    int v = 0, si = 0, sj = 0, ei = 0, ej = 0;
    vector<vector<int>> maze;
    int i = 0;
    for (string s; getline(infile, s); ) {
        //printf("s: %s\n", s.c_str());
        if (s.c_str()[0] != '#') {
            printf("-> Skipping row\n");
            continue;
        }
        vector<int> row;
        for(int j = 0; j < s.size(); ++j) {
            if (s[j]=='o') {
                si=i;
                sj=j;
                row.push_back(0);
            } else if(s[j]=='!') {
                ei=i;
                ej=j;
                row.push_back((1<<30));
            } else {
                row.push_back((s[j]=='#'?-1:(1<<30)));
            }
        }
        maze.push_back(row);
        ++i;
    }

    solve(maze, si, sj, ei, ej);
    //print2D(maze);
    //cout << "Shortest distance to end: " << maze[ei][ej] << endl;
    return 0;
}
